import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BalanceComponent } from './balance/balance.component';
import { DepositComponent } from './deposit/deposit.component';
import { FundTransferComponent } from './fund-transfer/fund-transfer.component';
import { ProfileComponent } from './profile/profile.component';
import { StatementComponent } from './statement/statement.component';
import { WithdrawComponent } from './withdraw/withdraw.component';

const routes: Routes = [
  {path:'profile', component:ProfileComponent},
  {path:'deposit', component:DepositComponent},
  {path:'withdraw', component:WithdrawComponent},
  {path:'transfer', component:FundTransferComponent},
  {path:'balance', component: BalanceComponent},
  {path:'statement', component:StatementComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [ProfileComponent, DepositComponent, WithdrawComponent, FundTransferComponent, BalanceComponent, StatementComponent];
